﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using elearning_net.Models; 

namespace elearning_net.Controllers
{
    public class CustomerController : Controller
    {

        String api_name = "customer"; 

        // GET: Customer  
        public ActionResult Index()
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            ViewBag.listCustomers = CustomerModelAPI.getAll(api_name); 

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Add");
        }
        [HttpPost]
        public ActionResult Add(CustomerModelSearch cvm)
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            CustomerModelAPI.post(api_name, cvm.customer);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Detail(String id)
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            CustomerModelSearch CVM = new CustomerModelSearch();
            CVM.customer = CustomerModelAPI.get(api_name, id);
            return View("Detail", CVM);
        }

        [HttpGet]
        public ActionResult Edit(String id)
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            CustomerModelSearch CVM = new CustomerModelSearch();
            CVM.customer = CustomerModelAPI.get(api_name,id);
            return View("Edit", CVM);
        }
        [HttpPost]
        public ActionResult Edit(CustomerModelSearch CVM)
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            CustomerModelAPI.put(api_name, CVM.customer);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(String id)
        {
            CustomerModelAPI CustomerModelAPI = new CustomerModelAPI();
            CustomerModelAPI.delete(api_name, id);
            return RedirectToAction("Index");
        }
    }

}