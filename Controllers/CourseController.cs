﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using elearning_net.Models; 

namespace elearning_net.Controllers
{
    public class CourseController : Controller
    {

        String api_name = "course"; 

        // GET: Course  
        public ActionResult Index()
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            ViewBag.listCourses = CourseModelAPI.getAll(api_name); 

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Add");
        }
        [HttpPost]
        public ActionResult Add(CourseModelSearch cvm)
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            CourseModelAPI.post(api_name, cvm.course);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Detail(String id)
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            CourseModelSearch CVM = new CourseModelSearch();
            CVM.course = CourseModelAPI.get(api_name, id);
            return View("Detail", CVM);
        }

        [HttpGet]
        public ActionResult Edit(String id)
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            CourseModelSearch CVM = new CourseModelSearch();
            CVM.course = CourseModelAPI.get(api_name,id);
            return View("Edit", CVM);
        }
        [HttpPost]
        public ActionResult Edit(CourseModelSearch CVM)
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            CourseModelAPI.put(api_name, CVM.course);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(String id)
        {
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            CourseModelAPI.delete(api_name, id);
            return RedirectToAction("Index");
        }
    }

}