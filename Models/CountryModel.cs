﻿using System;
using System.ComponentModel.DataAnnotations; 
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace elearning_net.Models
{
    public class CountryModel
    {
        [Display(Name = "id")]
        public string id { get; set; }
        [Display(Name = "country_name")]
        public string country_name { get; set; }
        [Display(Name = "phone_code")]
        public string phone_code { get; set; }
        [Display(Name = "country_code")]
        public string country_code { get; set; }
        [Display(Name = "createdAt")]
        public string createdAt { get; set; } 

    }


    public class CountryModelSearch
    {
        public CountryModel country { get; set; }
    }

    public class CountryModelAPI
    {

        API api = new API();

        public IEnumerable<CountryModel> getAll(String api_name)
        { 
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<CountryModel>>().Result;
                return null; 
            }
            catch
            {
                return null;
            }
        }


        public CountryModel get(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name + "/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<CountryModel>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool post(String api_name, CountryModel country)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PostAsJsonAsync(api_name, country).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool put(String api_name, CountryModel country)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PutAsJsonAsync(api_name + "/" + country.id, country).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool delete(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.DeleteAsync(api_name + "/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }


    }


}