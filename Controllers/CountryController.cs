﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using elearning_net.Models; 

namespace elearning_net.Controllers
{
    public class CountryController : Controller
    {

        String api_name = "country"; 

        // GET: Country  
        public ActionResult Index()
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            ViewBag.listCountrys = CountryModelAPI.getAll(api_name); 

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Add");
        }
        [HttpPost]
        public ActionResult Add(CountryModelSearch cvm)
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CountryModelAPI.post(api_name, cvm.country);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Detail(String id)
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CountryModelSearch CVM = new CountryModelSearch();
            CVM.country = CountryModelAPI.get(api_name, id);
            return View("Detail", CVM);
        }

        [HttpGet]
        public ActionResult Edit(String id)
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CountryModelSearch CVM = new CountryModelSearch();
            CVM.country = CountryModelAPI.get(api_name,id);
            return View("Edit", CVM);
        }
        [HttpPost]
        public ActionResult Edit(CountryModelSearch CVM)
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CountryModelAPI.put(api_name, CVM.country);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(String id)
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CountryModelAPI.delete(api_name, id);
            return RedirectToAction("Index");
        }
    }

}