﻿using System;
using System.ComponentModel.DataAnnotations; 
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace elearning_net.Models
{
    public class CourseModel
    {
        [Display(Name = "id")]
        public string id { get; set; }
        [Display(Name = "course_name")]
        public string course_name { get; set; } 
        [Display(Name = "createdAt")]
        public string createdAt { get; set; } 

    }


    public class CourseModelSearch
    {
        public CourseModel course { get; set; }
    }

    public class CourseModelAPI
    {

        API api = new API();

        public IEnumerable<CourseModel> getAll(String api_name)
        { 
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<CourseModel>>().Result;
                return null; 
            }
            catch
            {
                return null;
            }
        }


        public CourseModel get(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name + "/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<CourseModel>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool post(String api_name, CourseModel course)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PostAsJsonAsync(api_name, course).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool put(String api_name, CourseModel course)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PutAsJsonAsync(api_name + "/" + course.id, course).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool delete(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.DeleteAsync(api_name + "/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }


    }


}