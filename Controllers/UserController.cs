﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using elearning_net.Models;
using System.Web.Script.Serialization;

namespace elearning_net.Controllers
{
    public class UserController : Controller
    {

        String api_name1 = "country";
        String api_name2 = "course";
        String api_name3 = "user";

        // GET: Home  
        public ActionResult Index()
        { 
            CountryModelAPI CountryModelAPI = new CountryModelAPI();
            CourseModelAPI CourseModelAPI = new CourseModelAPI();
            UserModelAPI UserModelAPI = new UserModelAPI();

            ViewBag.listCountry = CountryModelAPI.getAll(api_name1);
            ViewBag.listCourse = CourseModelAPI.getAll(api_name2);
            ViewBag.listUser = UserModelAPI.getAll(api_name3);
            ViewBag.Message = TempData["shortMessage"].ToString();

            //string str = ((object)ViewBag.listUser).ToString(); 
            //var json = new JavaScriptSerializer().Serialize(ViewBag.listUser);
            //System.Diagnostics.Debug.WriteLine("=======================================================");
            //System.Diagnostics.Debug.WriteLine(str);
            //System.Diagnostics.Debug.WriteLine("=======================================================");

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            CountryModelAPI CountryModelAPI = new CountryModelAPI();  
            var country = CountryModelAPI.getAll(api_name1).ToList();
            SelectList listCountry = new SelectList(country, "id", "country_name");
            ViewBag.listCountry = listCountry;
            return View("Add");
        }
        [HttpPost]
        public ActionResult Add(UserModelSearch cvm)
        {
            UserModelAPI UserModelAPI = new UserModelAPI();
            UserModelAPI.post(api_name3, cvm.user);

            var json = new JavaScriptSerializer().Serialize(cvm.user);
            TempData["shortMessage"] =  json;

            System.Diagnostics.Debug.WriteLine("======================================================="); 
            System.Diagnostics.Debug.WriteLine(json);
            System.Diagnostics.Debug.WriteLine("=======================================================");
            return RedirectToAction("Index");
        }

    }

}