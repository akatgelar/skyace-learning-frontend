﻿using System;
using System.ComponentModel.DataAnnotations; 
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace elearning_net.Models
{
    public class CustomerModel
    {
        [Display(Name = "id")]
        public string id { get; set; }
        [Display(Name = "CustomerId")]
        public string CustomerId { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "MobileNo")]
        public string MobileNo { get; set; }
        [Display(Name = "Birthdate")] 
        public string Birthdate { get; set; }
        [Display(Name = "EmailId")]
        public string EmailId { get; set; }

    }


    public class CustomerModelSearch
    {
        public CustomerModel customer { get; set; }
    }

    public class CustomerModelAPI
    {

        API api = new API();

        public IEnumerable<CustomerModel> getAll(String api_name)
        { 
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<CustomerModel>>().Result;
                return null; 
            }
            catch
            {
                return null;
            }
        }


        public CustomerModel get(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name + "/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<CustomerModel>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool post(String api_name, CustomerModel customer)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PostAsJsonAsync(api_name, customer).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool put(String api_name, CustomerModel customer)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PutAsJsonAsync(api_name + "/" + customer.id, customer).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool delete(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.DeleteAsync(api_name + "/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }


    }


}