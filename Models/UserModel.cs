﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;

namespace elearning_net.Models
{
    public class UserModel
    {
        [Display(Name = "id")]
        public string id { get; set; }
        [Display(Name = "username")]
        public string username { get; set; }
        [Display(Name = "email")]
        public string email { get; set; }
        [Display(Name = "password")]
        public string password { get; set; }
        [Display(Name = "password_plain")]
        public string password_plain { get; set; }
        [Display(Name = "country")]
        public object country { get; set; }

    }


    public class UserModelSearch
    {
        public UserModel user { get; set; }
    }

    public class UserModelAPI
    {

        API api = new API();

        public IEnumerable<UserModel> getAll(String api_name)
        { 
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name).Result;

                System.Diagnostics.Debug.WriteLine(" what ? " + response.IsSuccessStatusCode);

                string json = response.Content.ReadAsStringAsync().Result;
                System.Diagnostics.Debug.WriteLine("=======================================================");
                System.Diagnostics.Debug.WriteLine(json);
                System.Diagnostics.Debug.WriteLine("=======================================================");

                //JavaScriptSerializer js = new JavaScriptSerializer();
                //dynamic movie = js.Deserialize<dynamic>(response.Content.ReadAsStringAsync().Result);

                //JavaScriptSerializer ser = new JavaScriptSerializer();
                //var jsonobj = ser.Deserialize<var>(json);

                //System.Diagnostics.Debug.WriteLine("=======================================================");
                //System.Diagnostics.Debug.WriteLine(movie);
                //System.Diagnostics.Debug.WriteLine("=======================================================");


                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<UserModel>>().Result;
                return null; 
            }
            catch
            {
                return null;
            }
        }


        public UserModel get(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.GetAsync(api_name + "/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<UserModel>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool post(String api_name, UserModel user)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PostAsJsonAsync(api_name, user).Result;
                System.Diagnostics.Debug.WriteLine("=======================================================");
                System.Diagnostics.Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
                System.Diagnostics.Debug.WriteLine("=======================================================");
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool put(String api_name, UserModel user)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.PutAsJsonAsync(api_name + "/" + user.id, user).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool delete(String api_name, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api.Base_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", api.jwt);
                HttpResponseMessage response = client.DeleteAsync(api_name + "/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }


    }


}